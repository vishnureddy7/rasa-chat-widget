#!/bin/bash

sudo docker stop meet-eng-chat-ui
sudo docker rm meet-eng-chat-ui
sudo docker build -t meet-eng-chat-ui ./
sudo docker run --name meet-eng-chat-ui -p 9123:9123 -d --restart=unless-stopped meet-eng-chat-ui
