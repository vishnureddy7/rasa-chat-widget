FROM ubuntu:latest

# update & upgrade system & apps
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y python3-pip python3-dev build-essential libssl-dev libffi-dev python3-setuptools
RUN pip3 install -U pip wheel

# create work directory
WORKDIR /app

# copy requirements and install
COPY requirements.txt .
RUN pip3 install -r requirements.txt

# copy the source directory
COPY . .

EXPOSE 9123

ENTRYPOINT [ "python3", "main.py" ]
